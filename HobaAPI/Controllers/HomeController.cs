﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace HobaAPI.Controllers
{
    public class HomeController : ApiController
    {
        [Route("api/uploadfile")]
        [HttpPost]
        public async Task<IHttpActionResult> UploadFiles([FromUri] UploadRequest request)
        {
            var response = new Response();
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var root = HttpContext.Current.Server.MapPath("~/files");
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            var provider = new CustomMultipartFormDataStreamProvider(root);

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

                // This illustrates how to get the file names.
                foreach (var file in provider.FileData)
                {
                    //var dbRequest = new Request()
                    //{
                    //    CreationDate = DateTime.Now,
                    //    FileName = file.Headers.ContentDisposition.FileName,
                    //    Username = request.Username
                    //};
                    Trace.WriteLine(file.Headers.ContentDisposition.FileName);
                    Trace.WriteLine("Server file path: " + file.LocalFileName);
                }
                return Ok(response);
            }
            catch (Exception e)
            {
                response.Succeded = false;
                response.Message = e.Message;
                return Ok(response);
            }
        }
    }

    public class CustomMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        public CustomMultipartFormDataStreamProvider(string path) : base(path)
        { }

        public override string GetLocalFileName(System.Net.Http.Headers.HttpContentHeaders headers)
        {
            var name = !string.IsNullOrWhiteSpace(headers.ContentDisposition.FileName) ? headers.ContentDisposition.FileName : "NoName";
            return name.Replace("\"", string.Empty);
            //this is here because Chrome submits files in quotation marks which get treated as part of the filename and get escaped
        }

    }
}
