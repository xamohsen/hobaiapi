﻿using System;

namespace HobaAPI
{
    public class Request
    {
        public DateTime CreationDate { get; set; }
        public string FileName { get; set; }
        public string Username { get; set; }
    }
}